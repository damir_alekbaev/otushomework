﻿namespace _23.HW.ParallelIntro
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Enter the path to the directory with the files (only files in the txt format will be checked)");
            var directory = Console.ReadLine();

            if (Directory.Exists(directory))
            {
                var parallelSpaceCounter = new ParallelSpaceCounter();
                await parallelSpaceCounter.ProcessDirectory(directory);
                await parallelSpaceCounter.ProcessDirectoryWithErrorHandling(directory);
            }
            else
            {
                Console.WriteLine($"Directory {directory} does not exist");
            }


        }
    }
}