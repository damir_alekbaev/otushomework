﻿
using System.Diagnostics;

namespace _23.HW.ParallelIntro
{

    internal class ParallelSpaceCounter
    {
        private Task executedTask = null;

        /// <summary>
        /// Process.txt files in directory in parallel and launch space count method WITHOUT Error Handling
        /// </summary>
        public async Task ProcessDirectory(string directoryPath)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var fileList = Directory.GetFiles(directoryPath, "*.txt").ToList();

            await Parallel.ForEachAsync(fileList, async (item, cancellationToken) =>
            {
                await GetCountOfSpacesAsync(item);
            });

            stopwatch.Stop();

            Console.WriteLine($"Execution time WITHOUT Error handling: {stopwatch.ElapsedMilliseconds}\n");
        }

        /// <summary>
        /// Process.txt files in directory in parallel and launch space count method WITH Error Handling
        /// </summary>
        public async Task ProcessDirectoryWithErrorHandling(string directoryPath)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var fileList = Directory.GetFiles(directoryPath, "*.txt").ToList();

            await Parallel.ForEachAsync(fileList, async (item, cancellationToken) =>
            {
                try
                {
                    var executedTask = GetCountOfSpacesAsync(item);
                    await executedTask;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception Message: {ex.Message}");
                    Console.WriteLine($"Exception Type: {ex.GetType()}");
                    Console.WriteLine($"Is Faulted: {executedTask.IsFaulted}");

                    if (executedTask.Exception is not null)
                    {
                        Console.WriteLine($"Exception Message: {executedTask.Exception?.InnerException.Message}");
                    }
                }
            });

            stopwatch.Stop();

            Console.WriteLine($"Execution time WITH Error handling: {stopwatch.ElapsedMilliseconds}");
        }

        /// <summary>
        /// Read the file and count spaces in it asynchronously  
        /// </summary>
        private async Task GetCountOfSpacesAsync(string filePath)
        {
            var fileName = Path.GetFileName(filePath);
            Console.WriteLine($"Start processing file {fileName}. Thread: {Thread.CurrentThread.ManagedThreadId}");

            using (var reader = File.OpenText(filePath)) 
            { 
                var text = await reader.ReadToEndAsync();
                var spaceCount = text.Count(Char.IsWhiteSpace);
                Console.WriteLine($"File \"{fileName}\" has {spaceCount} space\\s. Thread: {Thread.CurrentThread.ManagedThreadId}");
            }
            Console.WriteLine($"Finish processing file {fileName}. Thread: {Thread.CurrentThread.ManagedThreadId}");
        }
    }
}
