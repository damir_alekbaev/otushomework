﻿//#define TEST

using System.Runtime.InteropServices;
using Attributes;

#region Embded Attributes
SectionDivider("Embedded Attributes");

Console.WriteLine($"{typeof(A).Name} - {typeof(A).Attributes}");
Console.WriteLine($"{typeof(A).Name} - {typeof(A).GetTypeAttributes()}");

Console.WriteLine($"{typeof(ITest).Name} - {typeof(ITest).Attributes}");
Console.WriteLine($"{typeof(ITest).Name} - {typeof(ITest).GetTypeAttributes()}");

Console.WriteLine($"{typeof(Public).Name} - {typeof(Public).Attributes}");
Console.WriteLine($"{typeof(Public).Name} - {typeof(Public).GetTypeAttributes()}");

//Console.WriteLine($"{typeof(Struct).Name} - {typeof(Struct).Attributes}");
//Console.WriteLine($"{typeof(Struct).Name} - {typeof(Struct).GetTypeAttributes()}");

//Console.WriteLine($"{typeof(Static).Name} - {typeof(Static).Attributes}");
//Console.WriteLine($"{typeof(Static).Name} - {typeof(Static).GetTypeAttributes()}");

SectionDivider("",false);
#endregion

#region Method attributes
SectionDivider("Method attributes");

var m = typeof(A1).GetMethod("M1");
Console.WriteLine($"M1 Attributes - {typeof(A1).GetMethods().Single(x => x.Name == "M1").Attributes}");
Console.WriteLine($"M1 Attributes - {typeof(A1).GetMethods().Single(x => x.Name == "M1").GetMethodAttributes()}");

SectionDivider("", false);
#endregion

#region Custom, Debugging Attribute
SectionDivider("Custom, Debugging Attribute");

var p = new Public();

Console.WriteLine($"{p.GetType().Name} - " +
    $"{System.Attribute.GetCustomAttributes(p.GetType()).Aggregate("", (first, next) => $"{first} {next}") }");




SectionDivider("", false);
#endregion

#region Conditional Attribute
SectionDivider("Conditional Attribute");

var testC = new TestClass();

testC.C();
SectionDivider("", false);
#endregion


#region Layout in memory attributes
SectionDivider("Layout in memory attributes");

Console.WriteLine($"{typeof(Struct).Name} - {System.Attribute.GetCustomAttributes(typeof(Struct)).Aggregate("", (first, next) => $"{first} {next}")}");
Console.WriteLine($"{typeof(struct1).Name} - {System.Attribute.GetCustomAttributes(typeof(struct1)).Aggregate("", (first, next) => $"{first} {next}")}");


Console.WriteLine($"Size of Struct - {Marshal.SizeOf( new Struct())} byte");

Console.WriteLine($"Size of struct1 -  {Marshal.SizeOf( new struct1())}  byte");

SectionDivider("", false);
#endregion

#region Working with flag
SectionDivider("Working with flag memory attributes");

Console.WriteLine($"{DaysWithFlagAttrib.None.GetType().Name} - {System.Attribute.GetCustomAttributes(DaysWithFlagAttrib.None.GetType()).Aggregate("", (first, next) => $"{first} {next}") }");
Console.WriteLine($"{DaysWithoutFlagAttrib.None.GetType().Name} - {System.Attribute.GetCustomAttributes(DaysWithoutFlagAttrib.None.GetType()).Aggregate("", (first, next) => $"{first} {next}") }");

var weekendFlags = DaysWithFlagAttrib.Sun | DaysWithFlagAttrib.Sut;

var weekendWithoutFlags = DaysWithoutFlagAttrib.Sun | DaysWithoutFlagAttrib.Sut;

Console.WriteLine($"Weekend day [flag] is {weekendFlags}");
Console.WriteLine($"Weekend day is {weekendWithoutFlags}");
Console.WriteLine($"Weekend day is {weekendWithoutFlags.ToString("F")}");
//Console.WriteLine($"Working day [flag] is { DaysWithFlagAttrib.Workday.ToString("F")}");
//Console.WriteLine($"Working day is { DaysWithoutFlagAttrib.Workday}");
Console.WriteLine($"Monday  [flag]  is {DaysWithFlagAttrib.Mon}");
Console.WriteLine($"Monday is {DaysWithoutFlagAttrib.Mon}");

Console.WriteLine($"Monday HasFlag- NON is {DaysWithoutFlagAttrib.Mon.HasFlag((DaysWithoutFlagAttrib)0)}");

SectionDivider("", false);
#endregion

#region Working with custom attribute
SectionDivider("Working with custom attribute");

var p2 = new Public();
Console.WriteLine($"{p2.GetValueOfTestAttribute()}");

SectionDivider("", false);
#endregion

#region Get all attributes - additonal task
SectionDivider("Get all attributes - additonal task");


Console.WriteLine($"{SolverTask.SolveProblem()}");

SectionDivider("", false);
#endregion






void SectionDivider(String Name="", bool isStart=true)
{
    if(isStart)
        Console.WriteLine($"-----------------{Name} Section Start-----------------");
    else
        Console.WriteLine($" ");    

    
}