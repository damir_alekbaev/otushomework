﻿using System.Diagnostics;

namespace Attributes;

[DebuggerDisplay($"Dubugger {{i}} ")]
[MyTest("Test Attribute",AditionalProperty = "SomeProp")]
public class Public
{
    public int i { get; set; }

    public Public() {
        i = 2;
    }
   
    //[MyTest]
    private void TestMethod()
    {
        var j = 1;
    }
}