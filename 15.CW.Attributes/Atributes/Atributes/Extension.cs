﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Attributes;

[Flags]
public enum EnhancedTypeAttributes
{
    None = 1,

    NotPublic=2,
    Public=4,
    NestedPublic=8,        
    NestedPrivate=16,
    NestedFamily=32,
    NestedAssembly=64,
    NestedFamANDAssem=128,
    NestedFamORAssem=256,
    VisibilityTypeAttributes= NotPublic| Public| NestedPublic| NestedPrivate| NestedFamily| NestedAssembly| NestedFamANDAssem| NestedFamORAssem ,

    AutoLayout = 512,
    SequentialLayout =1024,
    ExplicitLayout = 2048,
    LayoutTypeAttributes= AutoLayout| SequentialLayout| ExplicitLayout,

    Class = 4096,
    Interface = 8192,
    SemanticTypeAttributes= Class| Interface,

    AnsiClass = 16384,
    UnicodeClass = 32768,
    AutoClass = 65536,
    CustomFormatClass = 131072,
    StringFormatTypeAttributes= AnsiClass| UnicodeClass| AutoClass| CustomFormatClass,     
   
    Abstract = 262_144,
    Sealed = 524_228,
    SpecialName = 1_048_576,
    RTSpecialName = 2_097_152,
    //
    // Summary:
    //     Specifies that the class or interface is imported from another module.
    Import = 4_194_304,
    //
    // Summary:
    //     Specifies that the class can be serialized.
    Serializable = 8_388_608,
    //
    // Summary:
    //     Specifies a Windows Runtime type.
    WindowsRuntime = 16_777_216,        
    //
    // Summary:
    //     Type has security associate with it.
    HasSecurity = 33_554_432,
    //
    // Summary:
    //     Attributes reserved for runtime use.
    BeforeFieldInit = 67_108_864,
    WithoutMaskTypeAttributes= Abstract| Sealed| SpecialName| RTSpecialName| Import| Serializable| WindowsRuntime| HasSecurity| BeforeFieldInit

}

[Flags]
public enum EnhancedMethodAttribute
{
    None=1,

    Private = 2,
    FamANDAssem = 4,
    Assembly = 8,
    Family = 16,
    FamORAssem = 32,
    Public = 64,
    VisibilityMethodAttributes = Private | Public | FamANDAssem | Assembly | FamORAssem | Public ,

    UnmanagedExport = 128,
    Static = 256,
    Final = 512,
    Virtual = 1024,
    HideBySig = 2048,


    NewSlot = 4096,
    ReuseSlot = 8192,


    CheckAccessOnOverride = 16384,
    Abstract = 32768,
    SpecialName = 65536,
    RTSpecialName = 131072,

    PinvokeImpl = 262_144,
    HasSecurity = 524_228,
    RequireSecObject = 1_048_576,
    ReservedMask = 2_097_152,



}

public static class Extensions
{
    public static EnhancedTypeAttributes GetTypeAttributes(this Type type)
    {
        EnhancedTypeAttributes enhancedTypeAttributes = EnhancedTypeAttributes.None;

        if (type.IsNotPublic) enhancedTypeAttributes |= EnhancedTypeAttributes.NotPublic;
        if (type.IsPublic) enhancedTypeAttributes |= EnhancedTypeAttributes.Public;
        if (type.IsNestedPublic) enhancedTypeAttributes |= EnhancedTypeAttributes.NestedPublic;
        if (type.IsNestedPrivate) enhancedTypeAttributes |= EnhancedTypeAttributes.NestedPrivate;
        if (type.IsNestedFamily) enhancedTypeAttributes |= EnhancedTypeAttributes.NestedFamily;
        if (type.IsNestedAssembly) enhancedTypeAttributes |= EnhancedTypeAttributes.NestedAssembly;
        if (type.IsNestedFamANDAssem) enhancedTypeAttributes |= EnhancedTypeAttributes.NestedFamANDAssem;
        if (type.IsNestedFamORAssem) enhancedTypeAttributes |= EnhancedTypeAttributes.NestedFamORAssem;

        if (type.IsAutoLayout) enhancedTypeAttributes |= EnhancedTypeAttributes.AutoLayout;
        if ((type.Attributes & TypeAttributes.SequentialLayout) == TypeAttributes.SequentialLayout) enhancedTypeAttributes |= EnhancedTypeAttributes.SequentialLayout;
        if (type.IsExplicitLayout) enhancedTypeAttributes |= EnhancedTypeAttributes.ExplicitLayout;

        if (type.IsClass) enhancedTypeAttributes |= EnhancedTypeAttributes.Class;
        if (type.IsInterface) enhancedTypeAttributes |= EnhancedTypeAttributes.Interface;

        if (type.IsAnsiClass) enhancedTypeAttributes |= EnhancedTypeAttributes.AnsiClass;
        if (type.IsUnicodeClass) enhancedTypeAttributes |= EnhancedTypeAttributes.UnicodeClass;
        if (type.IsAutoClass) enhancedTypeAttributes |= EnhancedTypeAttributes.AutoClass;
        if ((type.Attributes & TypeAttributes.CustomFormatClass) == TypeAttributes.CustomFormatClass) enhancedTypeAttributes |= EnhancedTypeAttributes.CustomFormatClass;

        if (type.IsAbstract) enhancedTypeAttributes |= EnhancedTypeAttributes.Abstract;
        if (type.IsSealed) enhancedTypeAttributes |= EnhancedTypeAttributes.Sealed;
        if (type.IsSpecialName) enhancedTypeAttributes |= EnhancedTypeAttributes.SpecialName;
        if ((type.Attributes & TypeAttributes.RTSpecialName) == TypeAttributes.RTSpecialName) enhancedTypeAttributes |= EnhancedTypeAttributes.RTSpecialName;
        if (type.IsImport) enhancedTypeAttributes |= EnhancedTypeAttributes.Import;
        if (type.IsSerializable) enhancedTypeAttributes |= EnhancedTypeAttributes.Serializable;
        if ((type.Attributes & TypeAttributes.WindowsRuntime) == TypeAttributes.WindowsRuntime) enhancedTypeAttributes |= EnhancedTypeAttributes.WindowsRuntime;
        if ((type.Attributes & TypeAttributes.HasSecurity) == TypeAttributes.HasSecurity) enhancedTypeAttributes |= EnhancedTypeAttributes.HasSecurity;
        if ((type.Attributes & TypeAttributes.BeforeFieldInit) == TypeAttributes.BeforeFieldInit) enhancedTypeAttributes |= EnhancedTypeAttributes.BeforeFieldInit;

        if ((enhancedTypeAttributes & EnhancedTypeAttributes.None) != 0) enhancedTypeAttributes &= (~EnhancedTypeAttributes.None);

        return enhancedTypeAttributes;
    }

    public static EnhancedMethodAttribute GetMethodAttributes(this MethodBase method)
    {
        EnhancedMethodAttribute enhancedMethodAttributes = EnhancedMethodAttribute.None;
        MethodAttributes methodAttributes;

        if (method.IsPrivate) enhancedMethodAttributes |= EnhancedMethodAttribute.Private;
        if (method.IsFamilyAndAssembly) enhancedMethodAttributes |= EnhancedMethodAttribute.FamANDAssem;
        if (method.IsAssembly) enhancedMethodAttributes |= EnhancedMethodAttribute.Assembly;
        if (method.IsFamily) enhancedMethodAttributes |= EnhancedMethodAttribute.Family;
        if (method.IsFamilyOrAssembly) enhancedMethodAttributes |= EnhancedMethodAttribute.FamORAssem;
        if (method.IsPublic) enhancedMethodAttributes |= EnhancedMethodAttribute.Public;
        if ((method.Attributes & MethodAttributes.UnmanagedExport)==MethodAttributes.UnmanagedExport) enhancedMethodAttributes |= EnhancedMethodAttribute.UnmanagedExport;
        if (method.IsStatic) enhancedMethodAttributes |= EnhancedMethodAttribute.Static;

        if (method.IsFinal) enhancedMethodAttributes |= EnhancedMethodAttribute.Final;
        if (method.IsVirtual) enhancedMethodAttributes |= EnhancedMethodAttribute.Virtual;
        if (method.IsHideBySig) enhancedMethodAttributes |= EnhancedMethodAttribute.HideBySig;

        if ((method.Attributes & MethodAttributes.NewSlot) == MethodAttributes.NewSlot) enhancedMethodAttributes |= EnhancedMethodAttribute.NewSlot;
        //if (method.IsReuseSlot) enhancedMethodAttributes |= EnhancedMethodAttribute.ReuseSlot;

        if ((method.Attributes & MethodAttributes.CheckAccessOnOverride) == MethodAttributes.CheckAccessOnOverride) enhancedMethodAttributes |= EnhancedMethodAttribute.CheckAccessOnOverride;
        if (method.IsAbstract) enhancedMethodAttributes |= EnhancedMethodAttribute.Abstract;
        if (method.IsSpecialName) enhancedMethodAttributes |= EnhancedMethodAttribute.SpecialName;
        if ((method.Attributes & MethodAttributes.RTSpecialName) == MethodAttributes.RTSpecialName) enhancedMethodAttributes |= EnhancedMethodAttribute.RTSpecialName;

        if ((method.Attributes & MethodAttributes.PinvokeImpl) == MethodAttributes.PinvokeImpl) enhancedMethodAttributes |= EnhancedMethodAttribute.PinvokeImpl;
        if ((method.Attributes & MethodAttributes.HasSecurity) == MethodAttributes.HasSecurity) enhancedMethodAttributes |= EnhancedMethodAttribute.HasSecurity;
        if ((method.Attributes & MethodAttributes.RequireSecObject) == MethodAttributes.RequireSecObject) enhancedMethodAttributes |= EnhancedMethodAttribute.RequireSecObject;
        if ((method.Attributes & MethodAttributes.ReservedMask) == MethodAttributes.ReservedMask) enhancedMethodAttributes |= EnhancedMethodAttribute.ReservedMask;
      

        if ((enhancedMethodAttributes & EnhancedMethodAttribute.None) != 0) enhancedMethodAttributes &= (~EnhancedMethodAttribute.None);

        return enhancedMethodAttributes;
    }

    public static int GetSetBitCount(long lValue)
    {
        int iCount = 0;

        //Loop the value while there are still bits
        while (lValue != 0)
        {
            //Remove the end bit
            lValue = lValue & (lValue - 1);

            //Increment the count
            iCount++;
        }

        //Return the count
        return iCount;
    }
}
