﻿using System.Runtime.InteropServices;

namespace Attributes;

//[StructLayout(LayoutKind.Explicit)]
//[StructLayout(LayoutKind.Auto)]
[StructLayout(LayoutKind.Sequential, Pack=2)]
struct Struct
{
    public byte a; // 1 byte
    public int b; // 4 bytes
    public short c; // 2 bytes
    public byte d; // 1 byte
}

public struct struct1
{
    public byte a; // 1 byte
    public int b; // 4 bytes
    public short c; // 2 bytes
    public byte d; // 1 byte
}