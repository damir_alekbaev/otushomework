﻿namespace Attributes;

[Flags]
public enum DaysWithFlagAttrib:short
{
    None=0,
    Mon=1,
    Tue=2,
    Wed=4,
    Thu=8,
    Fri=16,
    Sut=32,
    Sun=64,
    //Workday=Mon|Tue|Wed|Thu|Fri,
    //Weekend=Sut|Sun
}

public enum DaysWithoutFlagAttrib:short
{
    None = 0,
    Mon = 1,
    Tue = 2,
    Wed = 4,
    Thu = 8,
    Fri = 16,
    Sut = 32,
    Sun = 64,
    //Workday = Mon | Tue | Wed | Thu | Fri,
    //Weekend = Sut | Sun
}