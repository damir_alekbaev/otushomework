<Query Kind="Program" />

void Main()
{
	var odd=Numbers.first|Numbers.third;   //0b0101
	var even=Numbers.second|Numbers.forth;  //0b1010
	var another=Numbers.second|Numbers.forth|Numbers.third;  //0b1110
	((odd&Numbers.first)==Numbers.first).Dump();
	odd.HasFlag(Numbers.first).Dump();
	odd.HasFlag((Numbers) 0).Dump(); 
	odd.HasFlag(even).Dump();
}

// You can define other methods, fields, classes and namespaces here

enum Numbers:short{
	first  =  0b0001,
	second =  0b0010,
	third  =  0b0100,
	forth  =  0b1000
}
0b0001
0b0100

0b0101
0b0001

0b0001