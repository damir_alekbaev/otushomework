<Query Kind="Program" />

void Main()
{
	var p = new Public();
	
	var attribute=System.Attribute.GetCustomAttribute(p.GetType(),typeof(MyTestAttribute));
	
	Console.WriteLine(attribute.ToString());
	
	PropertyInfo pi=attribute.GetType().GetProperty("AditionalProperty");
	
	Console.WriteLine(pi.GetValue(attribute));

	Console.WriteLine($"{p.GetType().Name} - {System.Attribute.GetCustomAttributes(p.GetType()).Aggregate("", (first, next) => $"{first} {next}")}");
	

}

[DebuggerDisplay($"Dubugger {{i}} ")]
[MyTest("Test Attribute", AditionalProperty = "SomeProp")]
public class Public
{
	public int i { get; set; }

	public Public()
	{
		i = 2;
	}

	[MyTest]
	private void TestMethod()
	{
		var j = 1;
	}
	
	
}

[AttributeUsage(AttributeTargets.All)]
public class MyTestAttribute : System.Attribute
{
	public String Name { get; set; }
	public MyTestAttribute(string testAttribute = "")
	{
		Name = testAttribute;
	}

	public String AditionalProperty { get; set; }

	public override string ToString()
	{
		return $"{Name}-{AditionalProperty}";
	}
}