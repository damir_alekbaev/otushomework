<Query Kind="Program" />

void Main()
{
	var isAttributeDefined = typeof(UseAttribute).IsDefined(typeof(RemarkAttribute));
	isAttributeDefined.Dump();
	
	var remark = (typeof(UseAttribute).GetCustomAttribute(typeof(RemarkAttribute)) as RemarkAttribute).Remark;
	
	remark.Dump();
	
	//typeof(UseAttribute).GetCustomAttributesData().Single(x=>x.AttributeType==typeof(RemarkAttribute)).ConstructorArguments.First().Value.Dump(); 

}

// You can define other methods, fields, classes and namespaces here
[Remark("This class uses an attribute1.")]
public class UseAttribute  {}


[AttributeUsage(AttributeTargets.Class)]
public class RemarkAttribute : Attribute
{
	string _remark;

	public RemarkAttribute(string comment)
	{
		_remark = comment;
	}

	public string Remark
	{
		get { return _remark; }
	}
}
[AttributeUsage(AttributeTargets.Class)]
public class RemarkAttributeAttribute : Attribute
{
	string _remark;

	public RemarkAttributeAttribute(string comment)
	{
		_remark = comment;
	}

	public string Remark
	{
		get { return _remark; }
	}
}

