using Dapper;
using Npgsql;
using Otus.FileGetterService;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHostedService<MyBackgroundService>();
//builder.Services.AddHostedService<RabbitMqListener>();
var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseSwagger();
app.UseSwaggerUI(options =>
{
    options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    options.RoutePrefix = string.Empty;
});

app.MapPost("/api/users", (List<User> users) =>
{
    foreach (var u in users)
    {
        Console.WriteLine($"получил [{u.Id}] '{u.Login}'");
    }
});

app.MapGet("/api/users", () =>
{
    using (var con = new NpgsqlConnection("User ID=postgres;Password=postgres;Host=localhost;Port=5432;Database=otus-sql;"))
    {
        con.Open();

        var sql = @"select id as ""Id"", login as ""Login"" from fancy.users";
        var list = con.Query<User>(sql);
        return list;
    }
});



app.Run();
