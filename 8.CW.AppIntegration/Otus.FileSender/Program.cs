﻿using Dapper;
using Npgsql;
using NServiceBus;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

class Program
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }

    }


    private static List<User> GetUsers()
    {
        return new List<User>()
        {
            new (){Id=333, Login="apushkin4241241@mail.ru"},
            new (){Id=444, Login="gogolmog124124124ol@mail.ru"},
        };
    }

    private static void InsertUsers()
    {
        var sql = @"INSERT INTO fancy.users(id, login)
                    values(@Id, @Login)";

        using (var con = new NpgsqlConnection("User ID=postgres;Password=postgres;Host=localhost;Port=5432;Database=otus-sql;"))
        {
            con.Open();
            foreach (var user in GetUsers())
            {
                con.Execute(sql, user);
            }
        }

    }

    private static void CreateFileToShare()
    {
        var now = DateTime.Now;
        var path = $"C:\\otus\\file-{now:yyyy-MM-dd}.json";

        var users = GetUsers();

        var content = System.Text.Json.JsonSerializer.Serialize(users);

        File.WriteAllText(path, content);
    }

    private static async Task Send2Client()
    {
        try
        {
            var url = "http://localhost:5273/api/users";
            var client = new HttpClient();
            var users = GetUsers();

            var content = System.Text.Json.JsonSerializer.Serialize(users);

            var scontent = new StringContent(content, Encoding.UTF8, "application/json");

            var f = await client.PostAsync(url, scontent);
            Console.WriteLine(f.StatusCode);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
    }

    private static async Task PostQueueAsync()
    {
        var message = "PRIVET";
        using (var connection = GetRabbitConnection())
        using (var channel = connection.CreateModel())
        {
            channel.QueueDeclare(queue: "MyQueue",
                           durable: false,
                           exclusive: false,
                           autoDelete: false,
                           arguments: null);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: "",
                           routingKey: "MyQueue",
                           basicProperties: null,
                           body: body);
        }
    }

    static private IConnection GetRabbitConnection()
    {
        ConnectionFactory factory = new ConnectionFactory
        {
            UserName = "wzniqkas",
            Password = "",
            VirtualHost = "wzniqkas",
            HostName = "sparrow.rmq.cloudamqp.com"
        };
        IConnection conn = factory.CreateConnection();
        return conn;
    }


    public class UserMessage : IEvent
    {
        public User User { get; set; }
    }


    public static async Task Main()
    {
        await PostQueueAsync();
    }
}