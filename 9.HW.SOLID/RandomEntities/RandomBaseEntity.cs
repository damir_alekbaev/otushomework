﻿
namespace _9.HW.SOLID.Entities
{
    internal abstract class RandomBaseEntity<T> : IComparable
    {
        public abstract T Value { get; }

        public abstract int CompareTo(object? obj);
    }
}
