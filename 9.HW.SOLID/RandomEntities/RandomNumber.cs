﻿using _9.HW.SOLID.ConfigReaders;
using _9.HW.SOLID.Entities;

namespace _9.HW.SOLID.Entity
{
    internal class RandomNumber : RandomBaseEntity<int>
    {
        public override int  Value { get; }

        public RandomNumber(IConfigReadable config) 
        {
            Value = new Random().Next(0, config.Range);
        }

        public override int CompareTo(object? obj)
        {
            if (obj == null) return 1;

            return Value.CompareTo(Convert.ToInt32(obj));

        }
    }
}
