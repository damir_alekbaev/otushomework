﻿using _9.HW.SOLID.ConfigReaders;
using _9.HW.SOLID.Entities;

namespace _9.HW.SOLID.Comparers
{
    internal class RepeatableNumberComparer
    {
        public static void Comparer<T>(IConfigReadable config, RandomBaseEntity<T> baseEntity)
        {
            bool exitLoop = false;
            int attempts = config.Attempts;
            Console.WriteLine($"Guessing range is {config.Range}");
            do
            {
                Console.WriteLine("Enter your value: ");
                object? inputObject = Console.ReadLine();

                if (inputObject == null) throw new ArgumentException("Input value is null");

                switch (baseEntity.CompareTo(inputObject))
                {
                    case -1:
                        Console.WriteLine("Your value is bigger than guessed one");
                        break;
                    case 0:
                        Console.WriteLine("Congratulations you guessed the value");
                        exitLoop = true;
                        break;
                    case 1:
                        Console.WriteLine("Your value is less than guessed one");
                        break;
                }
                attempts--;
                Console.WriteLine($"You have {attempts} attempts.");

            } while (!exitLoop && attempts != 0);


            if (exitLoop)
                Console.WriteLine("You're lucky");
            else
                Console.WriteLine("You'll be lucky next time");
        }

    }
}
