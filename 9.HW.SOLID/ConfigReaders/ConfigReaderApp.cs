﻿using _9.HW.SOLID.ConfigReaders;
using System.Configuration;

namespace _9.HW.SOLID.CnfigReaders
{
    internal class ConfigReaderApp : IConfigReadable
    {
        public int Range { get; private set; }
        public int Attempts { get; private set; }
        public void GetConfigVaule()
        {
            Range  = Int32.Parse(ConfigurationManager.AppSettings.Get("range") ?? throw new ConfigurationErrorsException("Key 'range' does not exist in configuration"));
            Attempts = Int32.Parse(ConfigurationManager.AppSettings.Get("attempts") ?? throw new ConfigurationErrorsException("Key 'attempts' does not exist in configuration"));
        }
    }
}
