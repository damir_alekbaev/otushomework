﻿namespace _9.HW.SOLID.ConfigReaders
{
    internal interface IConfigReadable
    {
        public abstract int Range { get; }
        public abstract int Attempts { get; }

        public abstract void GetConfigVaule();

    }
}
