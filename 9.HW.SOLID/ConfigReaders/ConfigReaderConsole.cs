﻿using System.Configuration;


namespace _9.HW.SOLID.ConfigReaders
{
    internal class ConsoleConfigReader : IConfigReadable
    {
        public int Range { get; private set; }
        public int Attempts { get; private set; }

        public void GetConfigVaule()
        {
            Console.WriteLine("Enter the range for guessing number: ");
            Range = Int32.Parse(Console.ReadLine() ?? throw new ConfigurationErrorsException("Value for 'range' is empty"));
            Console.WriteLine("Enter the guessing attempts: ");
            Attempts = Int32.Parse(Console.ReadLine() ?? throw new ConfigurationErrorsException("Value for 'attempts' is empty"));
        }
    }
}
