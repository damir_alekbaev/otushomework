﻿using _9.HW.SOLID.CnfigReaders;
using _9.HW.SOLID.Comparers;
using _9.HW.SOLID.Entity;

namespace _9.HW.SOLID
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ConfigReaderApp config = new ConfigReaderApp();
            config.GetConfigVaule();
            RandomNumber randomNumber = new RandomNumber(config);
            
            RepeatableNumberComparer.Comparer(config, randomNumber);
        }
    }
}