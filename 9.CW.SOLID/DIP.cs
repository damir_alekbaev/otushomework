﻿namespace SolidNotGood
{
    public class Report
    {
    }

    public class ReportBuilder
    {
        public IList<Report> CreateReports()
        {
            //
            return new List<Report>();
        }
    }

    public class EmailReportSender
    {
        public void Send(Report report)
        {
            ///
        }
    }

    public class Reporter
    {
        public void SendReports()
        {
            var reportBuilder = new ReportBuilder();
            IList<Report> reports = reportBuilder.CreateReports();

            if (reports.Count == 0)
                throw new Exception("NoReports");

            var reportSender = new EmailReportSender();
            foreach (Report report in reports)
            {
                reportSender.Send(report);
            }
        }
    }
}