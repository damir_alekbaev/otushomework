﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace ConnectToDB.Abstractions
{
    public class BaseEntity
    {
        [BsonId]
        public Guid Id { get; set; }
    }
}
