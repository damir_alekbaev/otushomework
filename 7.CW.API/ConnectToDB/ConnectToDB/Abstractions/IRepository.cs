﻿namespace ConnectToDB.Abstractions
{
    public interface IRepository<T> where T : BaseEntity

    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T?> GetAsync(Guid id);
        Task AddAsynk(T entity);
        Task UpdateAsynk(T entity);
        Task DeleteAsynk(Guid id);
    }
}
