using ConnectToDB.Abstractions;
using ConnectToDB.Domain;
using ConnectToDB.Repos;
using EFCore.NamingConventions.Internal;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using Npgsql;
using System.Data.Common;
using System.Globalization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<DataContext>(x =>
{
    x.UseNpgsql(builder.Configuration.GetConnectionString("db"));
    x.UseLowerCaseNamingConvention();
});

builder.Services.AddScoped(typeof(DbContext), typeof(DataContext));
//PstgreSQL connection
//builder.Services.AddScoped(typeof(DbConnection), (_) => new NpgsqlConnection(builder.Configuration.GetConnectionString("db")));
//builder.Services.AddScoped(typeof(INameRewriter), (_) => new SnakeCaseNameRewriter(CultureInfo.CurrentCulture));
//MongoDB connection
builder.Services.AddScoped(typeof(MongoUrl), (_) => new MongoUrl(builder.Configuration.GetConnectionString("mongo")));
// Connection via EF Repository 
//builder.Services.AddScoped(typeof(IRepository<>),typeof(EFRepository<>));
// Connection via ADO Repository 
//builder.Services.AddScoped(typeof(IRepository<>), typeof(AdoRepository<>));
// Connection via Mongo Repository 
builder.Services.AddScoped(typeof(IRepository<>), typeof(MongoRepository<>));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
