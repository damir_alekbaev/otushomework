﻿using ConnectToDB.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace ConnectToDB.Domain
{
    public class DataContext:DbContext
    {
        public DbSet<User> Users { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) 
        {
            
        }
    }
}
