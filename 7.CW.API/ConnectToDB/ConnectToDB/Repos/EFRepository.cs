﻿using ConnectToDB.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace ConnectToDB.Repos
{
    public class EFRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DbContext _dataContext;
        public EFRepository(DbContext dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task AddAsynk(T entity)
        {
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsynk(Guid id)
        {
            var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if(entity != null) 
            { 
                _dataContext.Set<T>().Remove(entity);
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().ToListAsync();

        }

        public async Task<T?> GetAsync(Guid id)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

        }

        public async Task UpdateAsynk(T entity)
        {
            var entityOld = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == entity.Id);
            if (entityOld != null)
            {
                _dataContext.Entry(entityOld).CurrentValues.SetValues(entity);
                await _dataContext.SaveChangesAsync();
            }
        }
    }
}
