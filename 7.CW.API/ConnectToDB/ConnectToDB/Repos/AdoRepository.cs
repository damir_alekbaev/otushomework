﻿using ConnectToDB.Abstractions;
using EFCore.NamingConventions.Internal;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Npgsql;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace ConnectToDB.Repos
{
    public class AdoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DbConnection _connection;
        private readonly INameRewriter _nameRewriter;
        public AdoRepository(DbConnection connection, INameRewriter nameRewriter)
        {
            _connection = connection;
            _nameRewriter = nameRewriter;
            if(connection.State != System.Data.ConnectionState.Open)
                _connection.Open();
        }
        public async Task AddAsynk(T entity)
        {
            await using (var cmd = _connection.CreateCommand())
            {
                StringBuilder fields = new StringBuilder();
                StringBuilder values = new StringBuilder();
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                foreach(var elem in entity.GetType().GetProperties())
                {
                    if (fields.Length > 0 )
                    {
                        fields.Append(",");
                        values.Append(",");
                    }

                    var elemName = (elem.Name);
                    fields.Append(elemName);
                    values.Append($":{elemName}");
                    parameters.Add($"{elemName}", elem.GetValue(entity));
                }

                cmd.CommandText = $"INSERT INTO {typeof(T).Name.ToLower()}s ({fields}) VALUES ({values})";
                AddParameters(cmd, parameters);

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task DeleteAsynk(Guid id)
        {
            await using (var cmd = _connection.CreateCommand())
            {
                cmd.CommandText = $"DELETE FROM {typeof(T).Name.ToLower()}s WHERE Id = :UserId";
                AddParameters(cmd, new Dictionary<string, object>() {
                    {"UserId", id }
                });

                await cmd.ExecuteNonQueryAsync() ;
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = new List<T>();
            await using(var cmd = _connection.CreateCommand()) 
            {
                cmd.CommandText = $"SELECT * FROM {typeof(T).Name.ToLower()}s";

                await using(var reader = await cmd.ExecuteReaderAsync())
                {
                    while(reader.Read())
                    {
                        var item = Activator.CreateInstance<T>();
                        foreach(var elem in item.GetType().GetProperties())
                        {
                            elem.SetValue(item, reader[elem.Name]);
                        }
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public async Task<T?> GetAsync(Guid id)
        {
            T result = Activator.CreateInstance<T>();
            await using (var cmd = _connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT * FROM {typeof(T).Name.ToLower()}s WHERE id = :UserId";
                /*var parameter = cmd.CreateParameter();
                parameter.ParameterName = "UserId";
                parameter.Value = id;
                cmd.Parameters.Add(new NpgsqlParameter("UserId", id));
                cmd.Parameters.Add(parameter);*/
                AddParameters(cmd, new Dictionary<string, object>() {
                    {"UserId", id }
                });

                await using (var reader = await cmd.ExecuteReaderAsync())
                {
                    if (reader.Read())
                    {
                        foreach (var elem in result.GetType().GetProperties())
                        {
                            elem.SetValue(result, reader[elem.Name]);
                        }
                    }
                }
            }
            return result;
        }

        public async Task UpdateAsynk(T entity)
        {
            await using (var cmd = _connection.CreateCommand())
            {
                StringBuilder values = new StringBuilder();
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                foreach (var elem in entity.GetType().GetProperties().Where(x => x.Name != "Id"))
                {
                    if (values.Length > 0)
                    {
                        values.Append(",");
                    }

                    var elemName = elem.Name;
                    values.Append($"{elemName} = :{elemName}");
                    parameters.Add($"{elemName}", elem.GetValue(entity));
                }


                cmd.CommandText = $"UPDATE {typeof(T).Name.ToLower()}s SET {values} WHERE Id = :UserId";
                parameters.Add("UserId", entity.Id);
                AddParameters(cmd, parameters);

                await cmd.ExecuteNonQueryAsync();
            }

        }

        private void AddParameters(DbCommand cmd, Dictionary<string,object> parameters)
        {
            foreach(var item in parameters)
            {
                var parameter = cmd.CreateParameter();
                parameter.ParameterName = item.Key;
                parameter.Value = item.Value;
                cmd.Parameters.Add(parameter);
            }
        }
    }
}
