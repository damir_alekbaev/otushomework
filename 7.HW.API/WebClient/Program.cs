﻿using _4.OTUSBD.View.Enums;
using System;
using System.ComponentModel.Design;
using System.Reflection;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Select method (GETALL,GETBYID,INSERT,INSERTRANDOM): ");
                var method = Console.ReadLine();
                switch (Enum.Parse(typeof(MethodsEnum), method.ToUpper()))
                {
                    case MethodsEnum.GETALL:
                        {
                            var customers = CustomerRequest.CustomerGetAllRequest();
                            foreach (var customer in customers)
                            {
                                Console.WriteLine($"ID: {customer.Id}. First Name: {customer.Firstname}. Last Name: {customer.Lastname}.");
                            }
                            break;
                        }
                    case MethodsEnum.GETBYID:
                        {
                            Console.WriteLine("Pass the customer id:");
                            var id = Console.ReadLine();
                            var customer = CustomerRequest.CustomerGetByIdRequest(int.Parse(id));

                            Console.WriteLine($"ID: {customer.Id}. First Name: {customer.Firstname}. Last Name: {customer.Lastname}.");
                            break;
                        }
                    case MethodsEnum.INSERT:
                        {
                            Console.WriteLine("Pass the customer Id, First Name and Last Name in separate string:");

                            Customer customer = new Customer
                            {
                                Id = long.Parse(Console.ReadLine()),
                                Firstname = Console.ReadLine(),
                                Lastname = Console.ReadLine()
                            };

                            Customer newCustomer = CustomerRequest.CustomerCreateRequest(customer);
                            Console.WriteLine($"ID: {newCustomer.Id}. First Name: {newCustomer.Firstname}. Last Name: {newCustomer.Lastname}.");

                            break;
                        }
                    case MethodsEnum.INSERTRANDOM:
                        {
                            Customer customer = RandomCustomer();

                            Customer newCustomer = CustomerRequest.CustomerCreateRequest(customer);
                            Console.WriteLine($"ID: {newCustomer.Id}. First Name: {newCustomer.Firstname}. Last Name: {newCustomer.Lastname}.");
                            break;
                        }
                    default:
                        Console.WriteLine("Unexpected method.");
                        break;
                }

            }
        }

        private static Customer RandomCustomer()
        {
            Random random = new Random();
            return new Customer
            {
                
                Id = random.Next(),
                Firstname = $"String{random.Next()}",
                Lastname = $"String{random.Next()}"
            };
        }
    }
}