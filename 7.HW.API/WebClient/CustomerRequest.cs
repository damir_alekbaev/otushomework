using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace WebClient
{
    public static class CustomerRequest
    {
        private static HttpClient ApiConnectionInitialization()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:5001/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
        public static Customer CustomerCreateRequest(Customer customer)
        {
            HttpClient client = ApiConnectionInitialization();
            try
            {
                using (client)
                {
                    var request =  client.PostAsJsonAsync("customers", customer);
                    var response = request.Result.Content.ReadAsAsync<Customer>();

                    return response.Result;
                }
            }catch (Exception ex)
            { 
                Console.WriteLine($"Error: {ex.Message}");
                return null;    
            }
        }

        public static Customer CustomerGetByIdRequest(long id)
        {
            HttpClient client = ApiConnectionInitialization();
            try
            {
                using (client)
                {
                    var request = client.GetAsync($"customers/{id}");
                    var response = request.Result.Content.ReadAsAsync<Customer>();

                    return response.Result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return null;
            }
        }

        public static List<Customer> CustomerGetAllRequest()
        {
            HttpClient client = ApiConnectionInitialization();
            try
            {
                using (client)
                {
                    var request = client.GetAsync($"customers/all");
                    var response = request.Result.Content.ReadAsAsync<List<Customer>>();

                    return response.Result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return null;
            }
        }
    }

}