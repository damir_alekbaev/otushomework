﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Abstractions
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task AddAsync(T entity);
        Task DeleteAsync(long id);
        Task<IEnumerable<T>> GetAllAsync();
        Task<T?> GetAsync(long id);
        Task UpdateAsync(T entity);

    }
}
