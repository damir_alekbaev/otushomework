﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Abstractions
{
    public class BaseEntity
    {
        [Key]
        public long Id { get; init; }
    }
}
