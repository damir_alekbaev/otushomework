using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using WebApi.Abstractions;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomerController(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [HttpGet("{id:long}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Customer))]
        public async Task<IActionResult> GetCustomerAsync([FromRoute] long id)
        {
            var entity = _customerRepository.GetAsync(id);
            if (entity is not null)
            {
                return Ok(await entity);

            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {
            var entity = await _customerRepository.GetAsync(customer.Id);
            if (entity is null)
            {
                await _customerRepository.AddAsync(customer);
                return Ok(customer);
            }
            else
            {
                return Conflict();
            }

        }

        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Customer))]
        public async Task<IActionResult> GetAllCustomersAsync()
        {
            var entities =  await _customerRepository.GetAllAsync();
            if (entities is not null)
            {
                return Ok(entities);

            }
            else
            {
                return BadRequest();
            }

        }
    }
}