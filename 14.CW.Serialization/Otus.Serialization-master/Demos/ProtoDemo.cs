using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;

namespace Otus.Serializer.Demos
{
    public class ProtoDemo
    {
        public void Show()
        {
            // var r = new Random();
            // var l = new List<TestMe>();

            // for (var j = 0; j < 1000; j++)
            // {
            //     var t = new TestMe();
            //     for (var i = 0; i < 10000; i++)
            //     {
            //         t.Doubles.Add(r.NextDouble() * 10000);
            //     }
            //     l.Add(t);
            // }

            // var bf = new BinaryFormatter();
            // var sw = new Stopwatch();
            // sw.Start();
            // using (var fs = new FileStream("binary.bin", FileMode.Create))
            // {
            //     bf.Serialize(fs, l);
            // }
            // sw.Stop();

            // Console.WriteLine($"Binary: {sw.ElapsedMilliseconds}");

            // var model = RuntimeTypeModel.Create();
            // model.Add(typeof(List<TestMe>), true);

            // sw.Reset();
            // sw.Start();

            // using (var fs = new FileStream("proto.bin", FileMode.Create))
            // {
            //     model.Serialize(fs, l);
            // }
            // sw.Stop();

            // Console.WriteLine($"Proto: {sw.ElapsedMilliseconds}");

            using (var fs = new FileStream("ff.bin", FileMode.Create))
            {
                var bw = new BinaryWriter(fs);

                bw.Write(124);
                bw.Write(false);
                bw.Write("!!");
            }

            using (var fs = new FileStream("ff.bin", FileMode.Open))
            {
                var bw = new BinaryReader(fs);

                var i = bw.ReadInt32();
                var b = bw.ReadBoolean();
                var s = bw.ReadString();
                Console.WriteLine($"i = {i} b={b} s={s}");
            }
        }
    }

    [Serializable]
    [ProtoContract]
    public class TestMe
    {
        public TestMe()
        {
            Doubles = new List<double>();
        }

        [ProtoMember(1)]
        public List<double> Doubles { get; set; }

        [ProtoMember(2)]
        public double Foo { get; set; }
    }
}