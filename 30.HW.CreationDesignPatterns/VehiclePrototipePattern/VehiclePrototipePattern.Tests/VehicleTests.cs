﻿using NUnit.Framework;
using VehiclePrototipePattern.Entities;

namespace VehiclePrototipePattern.Tests
{
    internal class VehicleTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CloneCarTest()
        {
            Car originalCar = new Car { Wheels = 4, Power = 100, BodyType = "Sedan" };
            Car clonedCar = originalCar.Clone();

            Assert.That(originalCar.Wheels, Is.EqualTo(clonedCar.Wheels));
            Assert.That(originalCar.Power, Is.EqualTo(clonedCar.Power));
            Assert.That(originalCar.BodyType, Is.EqualTo(clonedCar.BodyType));
            Assert.That(originalCar, Is.Not.SameAs(clonedCar));
        }

        [Test]
        public void ElectricCarTest()
        {
            ElectricCar originalCar = new ElectricCar { Wheels = 4, Power = 100, BodyType = "Hatchback", BatteryCapacity = 500 };
            ElectricCar clonedCar = originalCar.Clone();

            Assert.That(originalCar.BatteryCapacity, Is.EqualTo(clonedCar.BatteryCapacity));
            Assert.That(originalCar, Is.Not.SameAs(clonedCar));

        }
    }
}
