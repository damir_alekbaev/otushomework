﻿using VehiclePrototipePattern.Entities;

namespace _30.HW.VehiclePrototipePattern
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car { Wheels = 4, Power = 100, BodyType = "Coupe" };
            Car clonedCar = car.Clone();

            Console.WriteLine("Original Car:");
            PrintVehicleDetails(car);

            Console.WriteLine("\nCloned Car:");
            PrintVehicleDetails(clonedCar);
        }

        private static void PrintVehicleDetails(Vehicle vehicle)
        {
            Console.WriteLine($"Wheels: {vehicle.Wheels}, Power: {vehicle.Power}, Object Hash Code: {vehicle.GetHashCode()}");

            if (vehicle is Car car)
            {
                Console.WriteLine($"Body Type: {car.BodyType}");
            }

            if (vehicle is ElectricCar electricCar)
            {
                Console.WriteLine($"Battery Capacity: {electricCar.BatteryCapacity}");
            }
        }
    }
}
