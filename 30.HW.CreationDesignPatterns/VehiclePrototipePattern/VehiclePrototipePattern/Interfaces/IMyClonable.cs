﻿
namespace VehiclePrototipePattern.Interfaces
{
    internal interface IMyClonable <T>
    {
        T Clone();
    }
}
