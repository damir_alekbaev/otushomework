﻿using VehiclePrototipePattern.Interfaces;

namespace VehiclePrototipePattern.Entities
{
    internal  class ElectricCar: Car, IMyClonable<ElectricCar>   
    {
        public int BatteryCapacity { get; set; }
        public new ElectricCar Clone()
        {
            return new ElectricCar
            {
                Wheels = this.Wheels,
                Power = this.Power,
                BatteryCapacity = this.BatteryCapacity,
            };
        }
    }
}
