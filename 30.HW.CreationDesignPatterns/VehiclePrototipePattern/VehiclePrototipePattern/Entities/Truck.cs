﻿using VehiclePrototipePattern.Interfaces;

namespace VehiclePrototipePattern.Entities
{
    internal class Truck: Vehicle, IMyClonable<Truck>
    {
        public int LoadCapacity { get; set; }
        public new Truck Clone()
        {
            return new Truck
            {
                Wheels = Wheels,
                Power = Power,
                LoadCapacity = LoadCapacity,
            };
        }
    }
}
