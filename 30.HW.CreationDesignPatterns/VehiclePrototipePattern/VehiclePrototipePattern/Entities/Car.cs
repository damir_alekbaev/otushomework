﻿using VehiclePrototipePattern.Interfaces;

namespace VehiclePrototipePattern.Entities
{
    internal class Car: Vehicle, IMyClonable<Car>   
    {
        public string BodyType { get; set; }
        public new Car Clone()
        {
            return new Car
            {
                Wheels = this.Wheels,
                Power = this.Power,
                BodyType = this.BodyType,
            };
        }
    }
}
