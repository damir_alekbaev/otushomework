﻿using VehiclePrototipePattern.Interfaces;

namespace VehiclePrototipePattern.Entities
{
    internal class Vehicle : IMyClonable<Vehicle>, ICloneable
    {
        public int Wheels { get; set; }
        public int Power { get; set; }

        public Vehicle Clone()
        {
            return new Vehicle
            {
                Wheels = this.Wheels,
                Power = this.Power
            };
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }
    }
}
