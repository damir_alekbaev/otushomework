﻿using _17.HW.Delegates.DirectoryLoop;
using _17.HW.Delegates.MaxValue;

namespace _17.HW.Delegates
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MaxValueRunner.Run();

            DirectoryLoopRunner.Run();
        }
    }
}