﻿using System;
using System.Collections.Generic;

namespace _17.HW.Delegates.MaxValue
{
    internal  class MaxValueRunner
    {
        /// <summary>
        /// The launcher method for checking the GetMax method
        /// </summary>
        public static void Run()
        {
            List<RandomNumber> randomNumbersList = new List<RandomNumber>();

            Console.Write("Below you will need to enter a number that indicates the length of the List that will be created and filled with Random values. " +
                "\nEnter a number: ");
            var listLength = Console.ReadLine();

            if (Int32.TryParse(listLength, out var parsedNumber))
            {
                randomNumbersList.AddRange(Enumerable.Range(0, parsedNumber).Select(_ => new RandomNumber()));
                Console.WriteLine("List of the Random values: ");
                randomNumbersList.ForEach(randomNumber => Console.Write($"{randomNumber.RandomNumberValue} | "));

                Console.WriteLine();
                Console.WriteLine($"Max value: {randomNumbersList.GetMax(RandomNumber.ComvertToFloat).RandomNumberValue}");
                Console.WriteLine();
            }else { Console.WriteLine($"{listLength} value cannot be parsed to the Integer."); }
        }
    }
}
