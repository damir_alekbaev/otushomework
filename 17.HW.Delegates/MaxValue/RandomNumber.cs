﻿namespace _17.HW.Delegates.MaxValue
{
    internal class RandomNumber
    {
        private Random random = new Random();
        public int RandomNumberValue { get; set; }

        public RandomNumber()
        {
            RandomNumberValue = random.Next(1, 500);
        }

        /// <summary>
        /// The method that converts RandomNumber class into a float
        /// </summary>
        public static float ComvertToFloat(RandomNumber number)
        {
            return Convert.ToSingle(number.RandomNumberValue);
        }
    }
}
