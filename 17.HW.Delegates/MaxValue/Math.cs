﻿namespace _17.HW.Delegates.MaxValue
{
    internal static class Math
    {
        /// <summary>
        /// An overridden method that calculates the maximum value from a collection
        /// </summary>

        public static T GetMax<T>(this IEnumerable<T> collecction, Func<T, float> convertToNumber) where T : class
        {
            T maxT = collecction.FirstOrDefault();
            foreach (var col in collecction)
            {
                if (convertToNumber(maxT) < convertToNumber(col))
                    maxT = col;
            }

            return maxT;
        }
    }
}
