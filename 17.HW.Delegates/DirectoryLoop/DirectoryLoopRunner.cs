﻿using System.Threading;

namespace _17.HW.Delegates.DirectoryLoop
{
    internal class DirectoryLoopRunner
    {
        static System.Timers.Timer _timer = new System.Timers.Timer(1000);

        /// <summary>
        /// The launcher method for checking the GoThroughtDirectory method
        /// </summary>
        public static void Run()
        {
            _timer.Elapsed += Timer_Elapsed;
            _timer.AutoReset = true;
            _timer.Start();

            Console.Write("Enter the path to the directory where you want to find all the files: ");
            string directoryPath = Console.ReadLine();
            if (Directory.Exists(directoryPath))
            {
                Console.WriteLine("The names of the files which were found in the directory are listed below:" +
                    "\n(If you would like to stop searching press ESC button)");

                ProcessDirectory processDirectory = new ProcessDirectory();

                processDirectory.FileHandler += ProcessDirectory.ShowOnConsole;
                processDirectory.GoThroughtDirectory(directoryPath);
            }
            else { Console.WriteLine($"Folder on provided path does not exist. Provided path: {directoryPath}"); }
        }

        /// <summary>
        /// The method that interrupts the operation throught loop in the Run method
        /// </summary>
        private static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            switch(Console.ReadKey(true).Key)
            {
                case ConsoleKey.Escape:
                    Console.WriteLine("Process has been interrupted forcely.");
                    System.Environment.Exit(0);
                    break;
            }
            Thread.Sleep(1000);

        }
    }
}
