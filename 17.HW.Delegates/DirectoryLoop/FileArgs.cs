﻿namespace _17.HW.Delegates.DirectoryLoop
{
    internal class FileArgs : EventArgs
    {
        public string Name { get; }
        public FileArgs(string name)
        {
            Name = name;
        }
    }
}
