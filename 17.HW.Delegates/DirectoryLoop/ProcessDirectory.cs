﻿namespace _17.HW.Delegates.DirectoryLoop
{
    internal class ProcessDirectory
    {
        public event EventHandler<FileArgs> FileHandler;

        /// <summary>
        /// The mathod  that goes through all the files in the folder and adds them to the event
        /// </summary>
        public void GoThroughtDirectory(string directoryPath)
        {
            foreach (string file in Directory.GetFiles(directoryPath))
            {
                FileHandler?.Invoke(this, new FileArgs(Path.GetFileName(file)));
            }
        }

        /// <summary>
        /// The mathod that shows file name on the console
        /// </summary>
        public static void ShowOnConsole(object sender, FileArgs fileArgs)
        {
            Console.WriteLine($"File name: {fileArgs.Name}");
        }
    }
}
