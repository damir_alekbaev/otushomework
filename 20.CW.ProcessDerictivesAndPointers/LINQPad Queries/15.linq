<Query Kind="Program" />

void Main()
{
	Test2();
}

void Test()
{
	Person? tom = null;
	try
	{
		tom = new Person("Tom");
	}
	finally
	{
		tom?.Dispose();
	}
}
void Test2()
{
	using Person tom = new Person("Tom");

	// переменная tom доступна только в блоке using
	// некоторые действия с объектом Person
	Console.WriteLine($"Name: {tom.Name}");
	Console.WriteLine($"Name: {tom.Name}");
	Console.WriteLine("The end of method Test2");
}

public class Person : IDisposable
{
	public string Name { get; }
	public Person(string name) => Name = name;

	public void Dispose() => Console.WriteLine($"{Name} has been disposed");
}