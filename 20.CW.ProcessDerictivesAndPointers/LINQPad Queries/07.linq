<Query Kind="Program" />

void Main()
{
	ShowSwap();
}

// You can define other methods, fields, classes and namespaces here
 void PSwap(ref int a, ref int b)
{
	int c = a;
	a = b;
	b = c;
}


private void ShowSwap()
{
	int a = 2, b = 3;
	PSwap(ref a, ref b);
	Console.WriteLine($"a={a} b={b}");
}