<Query Kind="Statements" />

#nullable disable
string? text = null; // здесь nullable-контекст не действует
Console.WriteLine(text);
#nullable restore

string? name = null;   // здесь nullable-контекст снова действует
Console.WriteLine(name);