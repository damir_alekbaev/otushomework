<Query Kind="Program" />

void Main()
{
	unsafe
	{
		int* x; // определение указателя
		int y = 10; // определяем переменную

		x = &y; // указатель x теперь указывает на адрес переменной y

		// получим адрес переменной y
		ulong addr = (ulong)x;
		Console.WriteLine($"Адрес переменной y: {addr}");
	}
}

// You can define other methods, fields, classes and namespaces here