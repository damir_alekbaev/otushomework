﻿namespace _28.HW.InProcessInteraction
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] randomOneHundredThousand = GetRandomArray(100_000);
            int[] randomMillion = GetRandomArray(1_000_000);
            int[] randomTenMillion = GetRandomArray(10_000_000);
            int[] randomOneHundredMillion = GetRandomArray(100_000_000);

            //Warming up
            Math.MeasureAverageExecutionTime(10,Math.CommonSum, randomOneHundredThousand);
            Math.MeasureAverageExecutionTime(10,Math.ParralelSum, randomOneHundredThousand);
            Math.MeasureAverageExecutionTime(10,Math.PLINQSum, randomOneHundredThousand);
            Math.MeasureAverageExecutionTime(10, Math.ThreadSum, randomOneHundredThousand);


            Console.WriteLine("OS: Windows 10 Pro");
            Console.WriteLine("Processor: Intel(R) Core(TM) i7-10610U CPU @ 1.80GHz   2.30 GHz");
            Console.WriteLine("RAM: 32.0 GB (31.6 GB usable)");
            Console.WriteLine();
            Console.WriteLine("{0,-40}| {1,-12}| {2,-12}| {3,-12}| {4,-12}|", 
                "Count of Elements", 
                "Common Sum", 
                "Parralel Sum",
                "PLINQ Sum",
                "Thread Sum");

            Console.WriteLine("{0,-40}| {1,12}| {2,12}| {3,12}| {4,12}|",
                "Random One Hundred Thousand Elements",
                Math.MeasureAverageExecutionTime(10,Math.CommonSum, randomOneHundredThousand),
                Math.MeasureAverageExecutionTime(10,Math.ParralelSum, randomOneHundredThousand),
                Math.MeasureAverageExecutionTime(10,Math.PLINQSum, randomOneHundredThousand),
                Math.MeasureAverageExecutionTime(10, Math.ThreadSum, randomOneHundredThousand));

            Console.WriteLine("{0,-40}| {1,12}| {2,12}| {3,12}| {4,12}|",
                "Random Millon Elements",
                Math.MeasureAverageExecutionTime(10, Math.CommonSum, randomMillion),
                Math.MeasureAverageExecutionTime(10, Math.ParralelSum, randomMillion),
                Math.MeasureAverageExecutionTime(10, Math.PLINQSum, randomMillion),
                Math.MeasureAverageExecutionTime(10, Math.ThreadSum, randomMillion));

            Console.WriteLine("{0,-40}| {1,12}| {2,12}| {3,12}| {4,12}|",
                "Random Ten Million Elements",
                Math.MeasureAverageExecutionTime(10, Math.CommonSum, randomTenMillion),
                Math.MeasureAverageExecutionTime(10, Math.ParralelSum, randomTenMillion),
                Math.MeasureAverageExecutionTime(10, Math.PLINQSum, randomTenMillion),
                Math.MeasureAverageExecutionTime(10, Math.ThreadSum, randomTenMillion));

            Console.WriteLine("{0,-40}| {1,12}| {2,12}| {3,12}| {4,12}|",
                "Random Hundred Million Elements",
                Math.MeasureAverageExecutionTime(10, Math.CommonSum, randomOneHundredMillion),
                Math.MeasureAverageExecutionTime(10, Math.ParralelSum, randomOneHundredMillion),
                Math.MeasureAverageExecutionTime(10, Math.PLINQSum, randomOneHundredMillion), 
                Math.MeasureAverageExecutionTime(10, Math.ThreadSum, randomOneHundredMillion));


        }

        /// <summary>
        /// Method for generating an array with random numbers
        /// </summary>

        public static int[] GetRandomArray(int maxElements)
        {
            int minValue = 0, maxValue = 10;
            Random random = new Random();
            int[] randomArray = Enumerable
                .Repeat(0,maxElements)
                .Select(i => random.Next(minValue,maxValue))
                .ToArray();
            return randomArray;
        }
    }
}