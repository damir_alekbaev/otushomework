﻿using System.Diagnostics;


namespace _28.HW.InProcessInteraction
{
    /// <summary>
    /// Class for calculating the sum of elements in a given array
    /// </summary>
    internal static class Math
    {
        /// <summary>
        /// Method that calculates the sum of the elements of an array by iterating through all the elements
        /// </summary>
        public static int CommonSum(int[] intArray)
        {

            int sum = 0;
            for (int i = 0; i < intArray.Length; i++)
            {
                sum += intArray[i];
            }

            return sum;
        }

        /// <summary>
        /// Method that calculates the sum of array elements by parallel summation
        /// </summary>
        public static int ParralelSum(int[] intArray)
        {
            int sum = 0;
            object lockObj = new object();

            Parallel.ForEach(intArray, () => 0, (item, state, localSum) =>
            {
                return localSum + item;
            },
                localSum =>
                {
                    lock (lockObj)
                    {
                        sum += localSum;
                    }
                });

            return sum;
        }

        /// <summary>
        /// Method that calculates the sum of array elements using the parallel LINQ method
        /// </summary>
        public static int PLINQSum(int[] intArray)
        {
            return intArray.AsParallel().Sum();
        }

        /// <summary>
        /// Method that measures the average execution time of summing elements in an array
        /// </summary>
        public static long MeasureAverageExecutionTime(int repetition, Func<int[], int> calculationMethod, int[] array)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            foreach (int i in Enumerable.Range(0, repetition))
            {
                calculationMethod(array);
            }
            stopwatch.Stop();

            return stopwatch.ElapsedTicks / repetition;
        }

        /// <summary>
        /// Method that calculates the sum of array elements using the parallel LINQ method
        /// </summary>
        public static int ThreadSum(int[] intArray)
        {
            int numberOfThreads = Environment.ProcessorCount; 
            int segmentSize = intArray.Length / numberOfThreads;
            int totalSum = 0;
            object lockObj = new object();
            CountdownEvent countdown = new CountdownEvent(numberOfThreads);

            for (int i = 0; i < numberOfThreads; i++)
            {
                int start = i * segmentSize;
                int end = (i == numberOfThreads - 1) ? intArray.Length : start + segmentSize;

                ThreadPool.QueueUserWorkItem(_ =>
                {
                    int segmentSum = SumArraySegment(intArray, start, end);
                    lock (lockObj)
                    {
                        totalSum += segmentSum;
                    }
                    countdown.Signal();
                });
            }

            countdown.Wait(); // Wait for all threads to finish
            return totalSum;

            // Local function to sum a segment of the array
            int SumArraySegment(int[] arr, int startIndex, int endIndex)
            {
                int sum = 0;
                for (int j = startIndex; j < endIndex; j++)
                {
                    sum += arr[j];
                }
                return sum;
            }

        }
    }
}
