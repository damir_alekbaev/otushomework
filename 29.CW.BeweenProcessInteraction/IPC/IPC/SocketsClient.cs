﻿using System;
using System.Diagnostics;
using System.IO.Pipes;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    public static class Program
    {
        private static byte[] buffer = new byte[512 * 1024];
        private static Stopwatch watch = new Stopwatch();
        private static long traffic = 0;
        private static int step = 0;

        private static void OnSocketReceive(IAsyncResult ar)
        {
            Socket client = (Socket)ar.AsyncState;

            traffic += client.EndReceive(ar);
            step++;

            if ((step % 1000) == 0)
            {
                watch.Stop();

                Console.WriteLine(
                  "{0} MB/s",
                  (1000 * (traffic >> 20)) / watch.ElapsedMilliseconds);

                watch.Start();
            }

            client.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, OnSocketReceive, client);
        }

        public static void Main(string[] args)
        {
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            client.ReceiveBufferSize = 2 * buffer.Length;
            client.Connect(new IPEndPoint(IPAddress.Loopback, 5000));

            watch.Start();

            client.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, OnSocketReceive, client);

            Console.WriteLine("CONNECTED");
            Console.ReadLine();
        }
    }
}
