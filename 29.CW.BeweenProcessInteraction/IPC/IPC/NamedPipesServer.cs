﻿using System;
using System.IO.Pipes;
using System.Net;
using System.Net.Sockets;

namespace ServerNP
{
    public static class Program
    {
        private static byte[] buffer = new byte[256 * 1024];

        private static void OnPipeSend(IAsyncResult ar)
        {
            NamedPipeServerStream server = (NamedPipeServerStream)ar.AsyncState;

            server.EndWrite(ar);
            server.BeginWrite(buffer, 0, buffer.Length, OnPipeSend, server);
        }

        public static void Main(string[] args)
        {
            NamedPipeServerStream server = new NamedPipeServerStream("TestPipe", PipeDirection.InOut, 1, PipeTransmissionMode.Byte, PipeOptions.Asynchronous, 1024 * 1024, 1024 * 1024);

            server.WaitForConnection();
            server.BeginWrite(buffer, 0, buffer.Length, OnPipeSend, server);

            Console.WriteLine("CONNECTED");
            Console.ReadLine();
        }
    }
}