﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsLog
{
    public class ReadEventLog
    {
        string app;
        public string AppName
        {
            get { return app; }
            set { app = "Application"; }
        }
        public void ReadApplicationLog()
        {
            // logType can be an Application, Security, System, or
            // any other Custom Log.
            string applicationlog = AppName;
            string mymachine = ".";   // local machine
            EventLog myapplicationLog = new EventLog(applicationlog,
               mymachine);
            EventLogEntryCollection entries =
               myapplicationLog.Entries;
            int LogCount = entries.Count;
            if (LogCount <= 0)
                Console.WriteLine("No Event Logs in the Log :" +
                  applicationlog);

            Console.WriteLine("Reading " + applicationlog + "Log");
            foreach (EventLogEntry entry in entries)
            {
                Console.WriteLine("################################");
                Console.WriteLine("Log Level: {0}", entry.EntryType);
                Console.WriteLine("Log Event id: {0}",
                   entry.InstanceId);
                Console.WriteLine("Log Message: {0}", entry.Message);
                Console.WriteLine("Log Source: {0}", entry.Source);
                Console.WriteLine("Entry Date: {0}",
                   entry.TimeGenerated);
                Console.WriteLine("################################");

            }
            Console.WriteLine("Finished Reading " + applicationlog +
               "Log");
        }

    }
}