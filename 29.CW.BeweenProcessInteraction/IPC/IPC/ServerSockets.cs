﻿using System;
using System.IO.Pipes;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    public static class Program
    {
        private static byte[] buffer = new byte[512 * 1024];

        private static void OnSocketSend(IAsyncResult ar)
        {
            Socket client = (Socket)ar.AsyncState;

            client.EndSend(ar);
            client.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, OnSocketSend, client);
        }

        public static void Main(string[] args)
        {
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
     
            server.Bind(new IPEndPoint(IPAddress.Loopback, 5000));
            server.Listen(10);

            while (true)
            {
                Socket client = server.Accept();

                client.SendBufferSize = 2 * buffer.Length;
                client.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, OnSocketSend, client);

                Console.WriteLine("CONNECTED");
            }
        }
    }
}