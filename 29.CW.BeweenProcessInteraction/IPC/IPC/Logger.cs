﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace WindowsLog
{
    static class Program
    {
        private static readonly ILogger log = new EventLogger();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRendering
                     Default(false);
                Application.Run(new Form1());
            }

            catch (Exception ex)
            {
                string source = "MySampleAppName";
                EventLog systemEventLog = new EventLog("Application");
                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, "Application");
                }
                log.Error("Exception in Main() method.", ex);
            }

        }

    }
}