﻿// This file is a "Hello, world!" in C# language by Mono for wandbox.
using System;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime.Serialization.Formatters.Binary;

namespace MMF
{
	[Serializable]  // mandatory
	class Message
	{
		public string title;
		public string content;
	}

	class Program
	{
		static void Main(string[] args)
		{
			const int MMF_MAX_SIZE = 1024;  // allocated memory for this memory mapped file (bytes)
			const int MMF_VIEW_SIZE = 1024; // how many bytes of the allocated memory can this process access

			// creates the memory mapped file which allows 'Reading' and 'Writing'
			MemoryMappedFile mmf = MemoryMappedFile.CreateOrOpen("mmf1", MMF_MAX_SIZE, MemoryMappedFileAccess.ReadWrite);

			// creates a stream for this process, which allows it to write data from offset 0 to 1024 (whole memory)
			MemoryMappedViewStream mmvStream = mmf.CreateViewStream(0, MMF_VIEW_SIZE);

			// this is what we want to write to the memory mapped file
			Message message1 = new Message();
			message1.title = "test";
			message1.content = "hello world";

			// serialize the variable 'message1' and write it to the memory mapped file
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Serialize(mmvStream, message1);
			mmvStream.Seek(0, SeekOrigin.Begin); // sets the current position back to the beginning of the stream

			// the memory mapped file lives as long as this process is running
			while (true) ;
		}
	}
}