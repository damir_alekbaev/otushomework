﻿// This file is a "Hello, world!" in C# language by Mono for wandbox.
using System;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime.Serialization.Formatters.Binary;

namespace MMFC
{
	[Serializable]  // mandatory
	class Message
	{
		public string title;
		public string content;
	}

	class Program
	{
		static void Main(string[] args)
		{
			const int MMF_MAX_SIZE = 1024;  // allocated memory for this memory mapped file (bytes)
			const int MMF_VIEW_SIZE = 1024; // how many bytes of the allocated memory can this process access

			// creates the memory mapped file
			MemoryMappedFile mmf = MemoryMappedFile.OpenExisting("mmf1");
			MemoryMappedViewStream mmvStream = mmf.CreateViewStream(0, MMF_VIEW_SIZE); // stream used to read data

			BinaryFormatter formatter = new BinaryFormatter();

			// needed for deserialization
			byte[] buffer = new byte[MMF_VIEW_SIZE];

			Message message1;

			// reads every second what's in the shared memory
			while (mmvStream.CanRead)
			{
				// stores everything into this buffer
				mmvStream.Read(buffer, 0, MMF_VIEW_SIZE);

				// deserializes the buffer & prints the message
				message1 = (Message)formatter.Deserialize(new MemoryStream(buffer));
				Console.WriteLine(message1.title + "\n" + message1.content + "\n");

				System.Threading.Thread.Sleep(1000);
			}
		}
	}
}