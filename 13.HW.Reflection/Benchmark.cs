﻿using _13.HW.Reflection.Convertors;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System.Diagnostics;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace _13.HW.Reflection
{
    [SimpleJob(RuntimeMoniker.Net70, 5, 5, 10, 3)]
    public class Benchmark
    {
        List<F> fList = Enumerable.Repeat(F.Get(), 10000).ToList();


        public void Show()
        {
            Action performanceTest = null;
            performanceTest += JsonSerialization;
            performanceTest += JsonDeserialization;
            performanceTest += CsvSerialization;
            performanceTest += CsvDeserialization;

            int count = 1000;

            foreach(var action in performanceTest.GetInvocationList())
            {
                //Warm up

                Enumerable.Range(0,10).ToList().ForEach(x => action.DynamicInvoke());

                //Performance test
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                Enumerable.Range(0, count).ToList().ForEach(x => action.DynamicInvoke());

                stopwatch.Stop();

                Console.WriteLine($"{action.Method.Name}: {stopwatch.ElapsedMilliseconds/ count}");
            }


            //File.Delete("ownXml.xml");
            //OwnXmlSerialization();
            //OwnXmlDeserialization();
        }

        [Benchmark]
        public void JsonSerialization()
        {
            var json = JsonSerializer.Serialize(fList, new JsonSerializerOptions { WriteIndented = true });
            File.WriteAllText("json.json", json);

        }

        [Benchmark]
        public void JsonDeserialization()
        {
            var after = JsonSerializer.Deserialize<List<F>>(File.ReadAllText("json.json"));
        }

        [Benchmark]
        public void OwnXmlSerialization()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var xml = StringConverter.Serialize(F.Get());
            File.WriteAllText("ownXml.xml", xml);


            stopwatch.Stop();

            Console.WriteLine($"xml ser: {stopwatch.ElapsedMilliseconds}");

            stopwatch.Reset();

        }

        [Benchmark]
        public void CsvSerialization()
        {
            CsvConverter<F> csvConverter = new CsvConverter<F>(',',"");
            var csv = csvConverter.Serialize(fList);
            File.WriteAllText("csv.csv", csv);
        }


        [Benchmark]
        public void CsvDeserialization()
        {

            CsvConverter<F> csvConverter = new CsvConverter<F>(',', "");
            var after = csvConverter.Deserialize(File.ReadAllText("csv.csv"));
        }
    }
}
