﻿using BenchmarkDotNet.Running;

namespace _13.HW.Reflection
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initial message!");
            //var summary = BenchmarkRunner.Run<Benchmark>();
            new Benchmark().Show();

            Console.WriteLine("End Performance testing.");
        }
    }
}