﻿
using System.Reflection;
using System.Text;

namespace _13.HW.Reflection.Convertors
{
    internal class StringConverter
    {
        public static String Serialize(object obj)
        {
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine($"<{obj.GetType().Name}>");
            IList<PropertyInfo> properties = obj.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                var propValue = property.GetValue(obj, null);
                sb.AppendLine($"<{property.Name}>{propValue}</{property.Name}>");
            }
            sb.AppendFormat($"</{obj.GetType().Name}>");

            return sb.ToString();
        }

    }
}
