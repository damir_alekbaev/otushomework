﻿

using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace _13.HW.Reflection.Convertors
{
    internal class CsvConverter<T> where T : class, new()
    {
        private char Separator { get; set; }
        private string Replacemet { get; set; }
        private List<PropertyInfo> _properties;

        public CsvConverter(char separator, string replacement) 
        {
            Separator = separator;
            Replacemet = replacement;
            var type = typeof(T);
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty);

            _properties = properties.Where(x => x.GetCustomAttribute<CsvIgnoreAttribute>() == null).OrderBy(x => x.Name).ToList();
        }

        private string GetHeader()
        {
            var columns = _properties.Select(x => x.Name).ToArray();
            var header = string.Join(Separator.ToString(), columns);
            return header;
        }
        public string Serialize(IEnumerable<T> data) 
        {
            if (data == null)
                throw new ArgumentNullException("Input parameter is null");


            var sb = new StringBuilder();
            var values = new List<string>();

            sb.AppendLine(GetHeader());

            var row = 1;
            foreach (var dataItem in data)
            {
                values.Clear();
                foreach(var p in _properties) 
                {
                    var rawData = p.GetValue(dataItem);
                    var value = rawData == null ? "" : rawData.ToString().Replace(Separator.ToString(), Replacemet);
                    values.Add(value);
                }

                sb.AppendLine(string.Join(Separator.ToString(), values));
            }

            return sb.ToString();
        }
        public IList<T> Deserialize(string data) 
        {
            string[] columns;
            string[] rows;

            using (var reader = new StringReader(data))
            {
                columns = reader.ReadLine().Split(Separator);
                rows = reader.ReadToEnd().Split(new String[] { Environment.NewLine }, StringSplitOptions.None);
            }

            var results = new List<T>();

            for(int row = 0; row < rows.Length-1; row++)
            {
                var line = rows[row];
                if (string.IsNullOrEmpty(line))
                {
                    throw new Exception(string.Format(@"Error: Empty line at line number: {0}", row));
                }

                var fields = line.Split(Separator);

                var tObject = new T();
                for (int i = 0; i < fields.Length; i++)
                {
                    var value = fields[i];
                    var column = columns[i];
                    var p = _properties.First(p => p.Name == column);
                    var converer = TypeDescriptor.GetConverter(p.PropertyType); 
                    var convertedValue = converer.ConvertFrom(value);

                    p.SetValue(tObject, convertedValue);
                }

                results.Add(tObject);
            }
            return results;
        }
    }
    
}
