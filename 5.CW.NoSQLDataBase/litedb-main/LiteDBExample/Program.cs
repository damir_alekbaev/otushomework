﻿using LiteDB;
using LiteDBExample;

// открывает базу данных, если ее нет - то создает
using (var db = new LiteDatabase(@"MyData.db"))
{
    // Получаем коллекцию
    var col = db.GetCollection<Company>("companies");

    var microsoft = new Company { Name = "Microsoft" };
    microsoft.Users = new List<User> { new User { Name = "Bill Gates" } };

    // Добавляем компанию в коллекцию
    col.Insert(microsoft);

    // Обновляем документ в коллекции
    microsoft.Name = "Microsoft Inc.";
    col.Update(microsoft);


    var google = new Company { Name = "Google" };
    google.Users = new List<User> { new User { Name = "Larry Page" } };
    col.Insert(google);

    // Получаем все документы
    var result = col.FindAll();
    foreach (Company c in result)
    {
        Console.WriteLine(c.Name);
        foreach (User u in c.Users)
            Console.WriteLine(u.Name);
        Console.WriteLine();
    }

    // Индексируем документ по определенному свойству
    col.EnsureIndex(x => x.Name);


    col.DeleteMany(x => x.Name == "Google");

    Console.WriteLine("После удаления Google");
    result = col.FindAll();
    foreach (Company c in result)
    {
        Console.WriteLine(c.Name);
        foreach (User u in c.Users)
            Console.WriteLine(u.Name);
        Console.WriteLine();
    }
}
Console.ReadKey();
