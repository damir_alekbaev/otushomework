﻿using _4.OTUSBD.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace _4.OTUSBD.Domain
{
    internal class DataContext:DbContext
    {
        public DbSet<Student> students { get; set; }
        public DbSet<Lecturer> lecturers { get; set;}
        public DbSet<Lesson>  lessons { get; set; }

        public DataContext()
        {
           //Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost:5432;Username=postgres;Password=admin;Database=OTUSHomework");
            base.OnConfiguring(optionsBuilder);
        }
    }
}
