﻿using _4.OTUSBD.Abstractions;

namespace _4.OTUSBD.Domain.Entities
{
    internal class Lecturer:BasePerson
    {
        /*[ForeignKey("LessonId")]
        public int LessonId { get; set; }*/
        public Lesson lesson { get; set; }
    }
}
