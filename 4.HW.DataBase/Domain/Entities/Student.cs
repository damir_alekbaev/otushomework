﻿using _4.OTUSBD.Abstractions;

namespace _4.OTUSBD.Domain.Entities
{
    internal class Student : BasePerson
    {
          public  IList<Lesson> lessons { get ; set; }
    }
}
