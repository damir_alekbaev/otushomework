﻿using _4.OTUSBD.Abstractions;

namespace _4.OTUSBD.Domain.Entities
{
    internal class Lesson:BaseEntity
    {
        public string Title { get; set; }
       
        public IList<Student> students { get; set; }

    }
}
