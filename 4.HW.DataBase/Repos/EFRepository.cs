﻿using _4.OTUSBD.Abstractions;
using _4.OTUSBD.Domain;
using Microsoft.EntityFrameworkCore;

namespace _4.OTUSBD.Repos
{
    internal class EFRepository<T> : IRepository<T> where T : BaseEntity
    {
        public EFRepository()
        {
        }
        public int AddItem(T entity)
        {
            using  (DbContext _dataContext = new DataContext())
            {
                _dataContext.Set<T>().Add(entity);
                return _dataContext.SaveChanges();
            }
        }

        public int DeleteItem(int Id)
        {
            using (DbContext _dataContext = new DataContext())
            {
                var entity =  _dataContext.Set<T>().FirstOrDefault(x => x.Id == Id);
                if (entity != null)
                {
                    _dataContext.Set<T>().Remove(entity);
                    return _dataContext.SaveChanges();
                }
                return 0;
            }
        }

        public List<T> GetAllItems()
        {
            using (DbContext _dataContext = new DataContext())
            {
                return _dataContext.Set<T>().Select(x => x).ToList();
            }
        }

        public T? GetItemById(int id)
        {
            using (DbContext _dataContext = new DataContext())
            {
                return  _dataContext.Set<T>().FirstOrDefault(x => x.Id == id);
            }
        }

        public int UpdateItem(T entity)
        {
            using (DbContext _dataContext = new DataContext())
            {
                var entityOld = _dataContext.Set<T>().FirstOrDefault(x => x.Id == entity.Id);
                if (entityOld != null)
                {
                    _dataContext.Entry(entityOld).CurrentValues.SetValues(entity);
                    return _dataContext.SaveChanges();
                }
                return 0;
            }
        }
    }
}
