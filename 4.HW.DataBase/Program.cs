﻿using _4.OTUSBD.Domain;
using _4.OTUSBD.Domain.Entities;
using _4.OTUSBD.Repos;
using _4.OTUSBD.View;
using _4.OTUSBD.View.Enums;
using _4.OTUSBD.Views;
using Microsoft.EntityFrameworkCore;

namespace _4.OTUSBD
{
    internal class Program
    {
        static void Main(string[] args)
        {
            do
            {
                using (DbContext _dataContext = new DataContext())
                {
                    if (_dataContext.Set<Student>().Select(x => x).ToList().Count == 0)
                        MockDataScript.FillDataTable();

                }

                ConsoleGeneric.DisplayAll();

                Console.Write("Select method (UPDATE,DELETE,INSERT): ");
                var methodString = Console.ReadLine();
                MethodsEnum methodsEnum;
                if(!Enum.TryParse(methodString, true, out methodsEnum))
                {
                    Console.WriteLine("Method is not suppoerted.");
                    continue;
                }

                Console.Write("Select table (lessons,students,lecturers): ");
                var tablesString = Console.ReadLine();
                TablesEnum tablesEnum;
                if (!Enum.TryParse(tablesString, true, out tablesEnum))
                {
                    Console.WriteLine("Table is not suppoerted.");
                    continue;
                }

                switch (tablesEnum)
                {

                    case TablesEnum.lecturers:
                        ConsoleLecturer.Execute(new EFRepository<Lecturer>(), methodsEnum);
                        break;
                    case TablesEnum.lessons:
                        ConsoleLesson.Execute(new EFRepository<Lesson>(), methodsEnum);
                        break;
                    case TablesEnum.students:
                        ConsoleStudent.Execute(new EFRepository<Student>(), methodsEnum);
                        break;
                    default:
                        Console.WriteLine("Table option is not supported.");
                        break;
                }


            } while (true);

        }
    }
}