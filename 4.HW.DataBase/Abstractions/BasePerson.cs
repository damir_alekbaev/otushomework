﻿namespace _4.OTUSBD.Abstractions
{
    internal class BasePerson : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
    }
}
