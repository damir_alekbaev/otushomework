﻿namespace _4.OTUSBD.Abstractions
{
    internal interface IRepository<T> where T : BaseEntity
    {
        List<T> GetAllItems();
        T? GetItemById(int id);
        int AddItem(T entity);
        int UpdateItem(T entity);
        int DeleteItem(int id);
    }
}
