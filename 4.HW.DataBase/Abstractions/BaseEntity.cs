﻿using System.ComponentModel.DataAnnotations;

namespace _4.OTUSBD.Abstractions
{
    internal class BaseEntity
    {
        [Key]
        public int Id{ get; set; }
    }
}
