﻿using _4.OTUSBD.Domain;
using _4.OTUSBD.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace _4.OTUSBD
{
    internal static class MockDataScript
    {
        public static void FillDataTable()
        {
            using (DbContext _dataContext = new DataContext())
            {
                List<Student> students = new List<Student>()
                {
                    new Student()
                    {
                        Id = 1,
                        FirstName = "Andrei",
                        MiddleName = "Dolgii",
                        LastName = "Nokolaevich",
                        Email = "Test@email.com"
                    },
                    new Student()
                    {
                        Id = 2,
                        FirstName = "Andrei",
                        MiddleName = "Ahmetov",
                        LastName = "Petrovich",
                        Email = "Test@email.com"
                    },
                    new Student()
                    {
                        Id = 3,
                        FirstName = "Andrei",
                        MiddleName = "Savin",
                        LastName = "Alexandrovich",
                        Email = "Test@email.com"
                    },
                    new Student()
                    {
                        Id = 4,
                        FirstName = "Andrei",
                        MiddleName = "Ivanov",
                        LastName = "Genadievich",
                        Email = "Test@email.com"
                    },
                    new Student()
                    {
                        Id = 5,
                        FirstName = "Andrei",
                        MiddleName = "Sirotkin",
                        LastName = "Petrovich",
                        Email = "Test@email.com"
                    },
                };
                _dataContext.Set<Student>().AddRange(students);
                _dataContext.SaveChanges();
            }

            using (DbContext _dataContext = new DataContext())
            {
                List<Lesson> lessons = new List<Lesson>()
                {
                    new Lesson()
                    {
                        Id = 1,
                        Title = "Programming"
                    },
                    new Lesson()
                    {
                        Id = 2,
                        Title = "API"
                    },
                    new Lesson()
                    {
                        Id = 3,
                        Title = "SQL DataBase"
                    },
                    new Lesson()
                    {
                        Id = 4,
                        Title = "NoSQL DataBase"
                    },
                    new Lesson()
                    {
                        Id = 5,
                        Title = "WebApi"
                    }



                };
                _dataContext.Set<Lesson>().AddRange(lessons);
                _dataContext.SaveChanges();
            }

            using(DbContext _dataContext = new DataContext())
            {
                List<Lecturer> lecturers = new List<Lecturer>()
                {
                    new Lecturer()
                    {
                        Id= 1,
                        FirstName = "Egor",
                        MiddleName ="Genadievich",
                        LastName = "Smith",
                        Email = "testotus@email.com",
                        lesson = _dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 1)

                    },
                    new Lecturer()
                    {
                        Id= 2,
                        FirstName ="Andrei",
                        MiddleName ="Georgievich",
                        LastName = "Perov",
                        Email = "testotus@email.com",
                        lesson = _dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 2)
                    },
                    new Lecturer()
                    {
                        Id= 3,
                        FirstName = "Petr",
                        MiddleName ="Stanislavovich",
                        LastName = "Sergeev",
                        Email = "testotus@email.com",
                        lesson = _dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 3)
                    },
                    new Lecturer()
                    {
                        Id= 4,
                        FirstName = "Vasili",
                        MiddleName ="Petrovich",
                        LastName = "Perov",
                        Email = "testotus@email.com",
                        lesson = _dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 4)
                    },
                    new Lecturer()
                    {
                        Id= 5,
                        FirstName = "Viktoria",
                        MiddleName ="Anderson",
                        LastName = "Clark",
                        Email = "testotus@email.com",
                        lesson = _dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 5)
                    }
                };
                _dataContext.Set<Lecturer>().AddRange(lecturers);
                _dataContext.SaveChanges();
            }

            using (DbContext _dataContext = new DataContext())
            {
                var student1 = _dataContext.Set<Student>().FirstOrDefault(x => x.Id == 1);
                student1.lessons = new List<Lesson>();
                student1.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 1));
                student1.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 2));

                var student2 = _dataContext.Set<Student>().FirstOrDefault(x => x.Id == 2);
                student2.lessons = new List<Lesson>();
                student2.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 1));
                student2.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 3));

                var student3 = _dataContext.Set<Student>().FirstOrDefault(x => x.Id == 3);
                student3.lessons = new List<Lesson>();
                student3.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 4));
                student3.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 5));

                var student4 = _dataContext.Set<Student>().FirstOrDefault(x => x.Id == 4);
                student4.lessons = new List<Lesson>();
                student4.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 1));
                student4.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 2));

                var student5 = _dataContext.Set<Student>().FirstOrDefault(x => x.Id == 4);
                student5.lessons = new List<Lesson>();
                student5.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 3));
                student5.lessons.Add(_dataContext.Set<Lesson>().FirstOrDefault(x => x.Id == 5));

                _dataContext.SaveChanges();
            }
        }
    }
}
