﻿using _4.OTUSBD.Domain.Entities;
using _4.OTUSBD.Repos;
using _4.OTUSBD.View.Enums;

namespace _4.OTUSBD.View
{
    internal static class ConsoleStudent
    {
        public static void Execute(EFRepository<Student> repository, MethodsEnum method)
        {
            try
            {
                switch (method)
                {

                    case MethodsEnum.INSERT:
                        ConsoleGeneric.InsertItem(repository, CreateItem());
                        Console.WriteLine("Item has been inserted");
                        break;
                    case MethodsEnum.UPDATE:
                        ConsoleGeneric.UpdateItem(repository, CreateItem());
                        Console.WriteLine("Item has been updated");
                        break;
                    case MethodsEnum.DELETE:
                        Console.Write($"Privide id of the item that should be deleted: ");
                        int id = Convert.ToInt32(Console.ReadLine());
                        ConsoleGeneric.DeletetItem(repository, id);
                        Console.WriteLine("Item has been deleted");
                        break;
                    default:
                        Console.WriteLine("Option is not supported.");
                        break;
                }
            }catch(Exception ex)
            {
                Console.WriteLine($"Exception occured in ConsoleStudent.Execute: {ex.Message}");
            }

        }
        private static Student CreateItem()
        {
            Console.WriteLine("Enter the parameters: ");
            Student student = new Student();
            Console.Write($"ID: ");
            student.Id = Convert.ToInt32(Console.ReadLine());
            Console.Write($"First Name: ");
            student.FirstName = Console.ReadLine();
            Console.Write($"Middle Name: ");
            student.MiddleName = Console.ReadLine();
            Console.Write($"Last Name: ");
            student.LastName = Console.ReadLine();
            Console.Write($"Email: ");
            student.Email = Console.ReadLine();

            List<Lesson> lessons = new List<Lesson>();

            Console.Write($"Do you want to add lecture? (y/n)");
            while (Console.ReadLine() == "y")
            {
                Lesson lesson = new Lesson();
                Console.Write($"Id: ");
                lesson.Id = Convert.ToInt32(Console.ReadLine());
                Console.Write($"title: ");
                lesson.Title = Console.ReadLine();
                lessons.Add( lesson );
                Console.Write($"Do you want to add lecture? (y/n)");
            }
            student.lessons = lessons;

            return student;
        }

    }
}
