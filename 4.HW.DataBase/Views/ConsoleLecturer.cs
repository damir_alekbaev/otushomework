﻿using _4.OTUSBD.Domain.Entities;
using _4.OTUSBD.Repos;
using _4.OTUSBD.View;
using _4.OTUSBD.View.Enums;

namespace _4.OTUSBD.Views
{
    internal static class ConsoleLecturer
    {
        public static void Execute(EFRepository<Lecturer> repository, MethodsEnum method)
        {
            try
            {
                switch (method)
                {

                    case MethodsEnum.INSERT:
                        ConsoleGeneric.InsertItem(repository, CreateItem());
                        Console.WriteLine("Item has been inserted");
                        break;
                    case MethodsEnum.UPDATE:
                        ConsoleGeneric.UpdateItem(repository, CreateItem());
                        Console.WriteLine("Item has been updated");
                        break;
                    case MethodsEnum.DELETE:
                        Console.Write($"Privide id of the item that should be deleted: ");
                        int id = Convert.ToInt32(Console.ReadLine());
                        ConsoleGeneric.DeletetItem(repository, id);
                        Console.WriteLine("Item has been deleted");
                        break;
                    default:
                        Console.WriteLine("Option is not supported.");
                        break;
                }
            }catch(Exception ex)
            {
                Console.WriteLine($"Exception occured in ConsoleLecturer.Execute: {ex.Message}");
            }

        }
        private static Lecturer CreateItem()
        {
            Console.WriteLine("Enter the parameters: ");
            Lecturer lecturer = new Lecturer();
            foreach (var property in lecturer.GetType().GetProperties())
            {
                Console.Write($"{property.Name}: ");
                var value = Console.ReadLine();
                property.SetValue(value, lecturer);
            }
            return lecturer;
        }
    }
}
