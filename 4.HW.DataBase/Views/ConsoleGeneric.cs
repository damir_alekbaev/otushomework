﻿using _4.OTUSBD.Abstractions;
using _4.OTUSBD.Domain.Entities;
using _4.OTUSBD.Repos;
using System.Text;

namespace _4.OTUSBD.View
{
    internal static class ConsoleGeneric
    {
        public static void DisplayAll()
        {
            IRepository<Student> studentRepository = new EFRepository<Student>();
            Print(studentRepository.GetAllItems());

            IRepository<Lecturer> lecturerRepository = new EFRepository<Lecturer>();
            Print(lecturerRepository.GetAllItems());

            IRepository<Lesson> lessonRepository = new EFRepository<Lesson>();
            Print(lessonRepository.GetAllItems());
        }
        public static void Print<T>(List<T> listOfItems)
        {
            StringBuilder result = new StringBuilder();
            result.Append(listOfItems.GetType().GetGenericArguments().Single().ToString().ToUpper());
            result.AppendLine();
            foreach (var item in listOfItems)
            {
                foreach (var property in item.GetType().GetProperties())
                {
                    result.Append($"| {property.Name}: {property.GetValue(item, null),15}");
                }
                result.AppendLine();
            }

            Console.WriteLine(result);
        }

        public static void DeletetItem<T>(EFRepository<T> repository, int id ) where T : BaseEntity
        { 
            try
            {
                repository.DeleteItem(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception occured in DeletetItem: {ex.Message}");
            }
        }

        public static void InsertItem<T>(EFRepository<T> repository, T entity) where T : BaseEntity
        {
            try
            {
                repository.AddItem(entity);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception occured in InsertItem: {ex.Message}");
            }
        }

        public static void UpdateItem<T>(EFRepository<T> repository, T entity) where T : BaseEntity
        {
            try
            {
                repository.UpdateItem(entity);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception occured in UpdateItem: {ex.Message}");
            }
        }

        /*
        public static void SelectAction()
        {
            Console.WriteLine("Select desired action (UPDATE,DELETE,INSERT):");
            var option = Console.ReadLine();
            switch (Enum.Parse(typeof(MethodsEnum), option))
            {
                case MethodsEnum.INSERT:
                    InsertItem();
                    break;
                case MethodsEnum.UPDATE:
                    break;
                case MethodsEnum.DELETE:
                    break;
                default:
                    Console.WriteLine("Option is not supported.");
                    break;
            }

        }

        public static void InsertItem()
        {
            Console.WriteLine("Select desired table (students, lecturers, lessons):");
            EFRepository<BaseEntity> repository;
            var table = Console.ReadLine();
            try
            {
                switch (Enum.Parse(typeof(TablesEnum), table))
                {
                    case TablesEnum.lecturers:
                        repository = new EFRepository<Lecturer>();
                        break;
                    case TablesEnum.lessons:
                        repository = new EFRepository<Lesson>();
                        break;
                    case TablesEnum.students:
                        repository = new EFRepository<Student>(); 
                        break;
                    default:
                        Console.WriteLine("Option is not supported.");
                        return null;
                        break;
                }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception occured: {ex.Message}");
            }
        }


        /public static EFRepository<T> SelectTables<T>() where T : BaseEntity
        {
            Console.WriteLine("Select desired table (students, lecturers, lessons):");
            var table = Console.ReadLine();
            switch (Enum.Parse(typeof(TablesEnum), table))
            {
                case TablesEnum.lecturers:
                    InsertItem();
                    return new EFRepository<Lecturer>();
                    break;
                case TablesEnum.lessons:
                    return new EFRepository<Lesson>();
                    break;
                case TablesEnum.students:
                    return new EFRepository<Student>();
                    break;
                default:
                    Console.WriteLine("Option is not supported.");
                    return null;
                    break;
            }

        }*/
    }
}
