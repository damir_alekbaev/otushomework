﻿using _4.OTUSBD.Domain.Entities;
using _4.OTUSBD.Repos;
using _4.OTUSBD.View.Enums;
using _4.OTUSBD.View;

namespace _4.OTUSBD.Views
{
    internal static class ConsoleLesson
    {
        public static void Execute(EFRepository<Lesson> repository, MethodsEnum method)
        {
            try
            {
                switch (method)
                {

                    case MethodsEnum.INSERT:
                        ConsoleGeneric.InsertItem(repository, CreateItem());
                        Console.WriteLine("Item has been inserted");
                        break;
                    case MethodsEnum.UPDATE:
                        ConsoleGeneric.UpdateItem(repository, CreateItem());
                        Console.WriteLine("Item has been updated");
                        break;
                    case MethodsEnum.DELETE:
                        Console.Write($"Privide id of the item  item that should be deleted: ");
                        int id = Convert.ToInt32(Console.ReadLine());
                        ConsoleGeneric.DeletetItem(repository, id);
                        Console.WriteLine("Item has been deleted");
                        break;
                    default:
                        Console.WriteLine("Option is not supported.");
                        break;
                }
            } catch(Exception ex)
            {
                Console.WriteLine($"Exception occured in ConsoleLesson.Execute: {ex.Message}");
            }

        }
        private static Lesson CreateItem()
        {
            Console.WriteLine("Enter the parameters: ");
            Lesson lesson = new Lesson();
            foreach (var property in lesson.GetType().GetProperties())
            {
                Console.Write($"{property.Name}: ");
                var value = Console.ReadLine();
                property.SetValue(value, lesson);
            }
            return lesson;
        }
    }
}
